with Last_Chance_Handler;  pragma Unreferenced (Last_Chance_Handler);
with mydraw;                   use mydraw;
with Peripherals_Nonblocking;  use Peripherals_Nonblocking;
with Serial_IO.Nonblocking;    use Serial_IO.Nonblocking;
with Message_Buffers;          use Message_Buffers;
with Types;                    use Types;
with Energy_Algorithm;         use Energy_Algorithm;
with Ada.Real_Time;            use Ada.Real_Time;
with Bpm;                      use Bpm;

package uart_nonblocking is
   function atoi (str : String) return Energy with
     Pre => (for all I in str'Range => str(I) in '0' .. '9'
             or else str(I) = Character'Val(10)
             or else str(I) = Character'Val(13));
   procedure get_uart;
end uart_nonblocking;
