package body mydraw is

   procedure Clear is
      BG : constant Bitmap_Color := (Alpha => 255, others => 64);
   begin
      Display.Hidden_Buffer (1).Set_Source (BG);
      Display.Hidden_Buffer (1).Fill;

      LCD_Std_Out.Clear_Screen;

      Display.Update_Layer (1, Copy_Back => True);
   end Clear;
   
   procedure Draw_init
is
   BG : constant Bitmap_Color := (Alpha => 255, others => 64);
begin

   --  Initialize LCD
   Display.Initialize;
   Display.Initialize_Layer (1, ARGB_8888);

   --  Initialize touch panel
   Touch_Panel.Initialize;

   --  Initialize button
   User_Button.Initialize;

   LCD_Std_Out.Set_Font (BMP_Fonts.Font16x24);
   LCD_Std_Out.Current_Background_Color := BG;

   --  Clear LCD (set background)
   Clear;
end Draw_init;

   procedure Draw_number (beat : String)
is
   
begin
      LCD_Std_Out.Put_Line (beat);
end Draw_number;

   procedure Draw_beat
is
   
begin
      LCD_Std_Out.Put_Line ("############");
end Draw_beat;
   
end mydraw;
