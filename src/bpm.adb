package body Bpm is
   function Init_Energy_Total return Energy_Table is
     Energy_Total : Energy_Table;
   begin
      for I in 1 .. Nb_Chunks loop
         Energy_Total(I) := 0;
      end loop;
      return Energy_Total;
   end Init_Energy_Total;

   function Init_Last_BPMs(Default : Float) return BPM_Table
   is
     Last_BPMs : BPM_Table;
   begin
      for I in 1 .. Inertia_Size loop
         Last_BPMs(I) := Default;
      end loop;
      return Last_BPMs;
   end Init_Last_BPMs;

   function Compute_Avg_Bpm(Last_BPMs : BPM_Table) return Float
   is
      Avg_Bpm : Float := 0.0;
   begin
      for I in 1 .. Inertia_Size loop
         Avg_Bpm := Avg_Bpm + Last_BPMs(I);
      end loop;
      return Avg_Bpm / Float(Inertia_Size);
   end Compute_Avg_Bpm;

   function Is_Beat (Energ        : in     Energy;
                     Energy_Total : in Out Energy_Table;
                     Id_Chunk     : in out Integer)
                     return Boolean
   is
      Etot : Float;
   begin
      Update_Energy(Energ, Energy_Total, Id_Chunk);
      Etot := Compute_Etot(Energy_Total);

      if Etot * Cst <= Float(Energ) then
         return True;
      end if;
      return False;
   end Is_Beat;

   procedure Update_Energy (Energ        : in     Energy;
                            Energy_Total : in Out Energy_Table;
                            Id_Chunk     : in out Integer)
   is
   begin
      Energy_Total(Id_Chunk) := Energ;
      Id_Chunk := Id_Chunk + 1;
      if Id_Chunk >= Nb_Chunks then
         Id_Chunk := 0;
      end if;
   end Update_Energy;

   function Compute_Etot (Energy_Total: in Energy_Table) return Float
   is
      Sum : Energy := 0;
   begin
      for I in 1 .. Nb_Chunks loop
         Sum := Sum + Energy_Total(I);
      end loop;
      return Float(Sum) / Float(Nb_Chunks);
   end Compute_Etot;

end Bpm;
