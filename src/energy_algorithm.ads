with Ada.Real_Time; use Ada.Real_Time;
with Types;         use Types;
with Bpm;           use Bpm;
with mydraw;        use mydraw;

package Energy_Algorithm is
   procedure Energy_Algo (New_Value      : in     Energy;
                          Last_Beat      : in out Boolean;
                          Id_Chunk       : in out Integer;
                          Last_BPMs      : in out BPM_Table;
                          Id_BPM         : in out Integer;
                          Tps, Last_Time : in out Time;
                          Last_Bad       : in out Boolean;
                          Energy_Total   : in out Energy_Table);
end Energy_Algorithm;
