#!/bin/env python3

import matplotlib.pyplot as plt
import numpy as np
import signal
import serial
import sys
import alsaaudio, time, audioop

from sys import argv

class col:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

stop = False
def signal_handler(sig, frame):
    print(col.WARNING, 'Ctrl+C signal detected', col.ENDC)
    global stop
    stop = True
    sys.exit(130)
signal.signal(signal.SIGINT, signal_handler)

inp = alsaaudio.PCM(alsaaudio.PCM_CAPTURE, alsaaudio.PCM_NORMAL)

inp.setchannels(1)
inp.setrate(20000)
inp.setformat(alsaaudio.PCM_FORMAT_S16_LE)
byte_size = 2

nb_sample = 1024
inp.setperiodsize(nb_sample)

plt.ion() # interactive matplotlib

nb_pts = 100
def main():
    global stop

    if len(argv) < 2:
        print(col.FAIL,'Missing argument')
        print(col.WARNING,'Usage: $./advanced_recorder.py serial', col.ENDC)
        return
    
    port_serie =  serial.Serial(port=argv[1], baudrate=115200, timeout=1
            , writeTimeout=1)
    if not port_serie.isOpen():
        print(col.FAIL,'invalid serial port', col.ENDC)
        return
    
    Cst = 1.35
    size = 44
    samples = np.zeros(size)
    ids = 0
    inertia_size = 15
    last_bpm = np.full(inertia_size, 100)
    id_bpm = 0
    timer = 0
    timer_no_sound = 0
    last = False

    last_very_bad = False
    last_good_time = 0
    while not(stop):
        data_str = ''
        l, data = inp.read()
        energ = 0
        if l:
            for k in range(nb_sample):
                val = audioop.getsample(data, byte_size, k)
                energ += val ** 2
            hex_nrg = "%0.16d" % energ
            hex_nrg += '\r\n'
            port_serie.write(hex_nrg.encode())
            vol = audioop.max(data, byte_size)
            if vol < 400:
                if time.perf_counter() - timer_no_sound > 3:
                    print(col.OKBLUE, "No sound..", col.ENDC)
                    continue
            else:
                timer_no_sound = time.perf_counter()
            
            samples[ids] = energ
            ids += 1
            if ids >= size:
                ids = 0

            e_tot = samples.sum() / size
            if Cst * e_tot < energ:
                last = True
                print("BEAT ------------")
            else:
                if last:
                    last = False
                    new_timer = time.perf_counter()
                    bpm = 1 / (new_timer - timer) * 60
                    tmp_time = timer
                    timer = new_timer

                    avg_bpm = last_bpm.sum() / inertia_size

                    if last_very_bad:
                        last_very_bad = False
                        alt_bpm = 1 / (new_timer - last_good_time) * 60
                        if abs(avg_bpm - alt_bpm) < 0.3 * avg_bpm:
                            bpm = alt_bpm
                        else:
                            if alt_bpm > avg_bpm:
                                print(col.FAIL, "STILL Rejected Beat:", avg_bpm, " : ", alt_bpm, col.ENDC)
                                continue
                            
                    if abs(avg_bpm - bpm) > max(0.6 * avg_bpm, 40) or bpm < 40:
                        print(col.FAIL, "Rejected Beat:", avg_bpm, " : ", bpm, col.ENDC)
                        last_very_bad = True
                        last_good_time = tmp_time
                        continue
                        
                    
                    last_bpm[id_bpm] = bpm
                    id_bpm += 1
                    if id_bpm >= inertia_size:
                        id_bpm = 0
                    if abs(avg_bpm - bpm) > 0.2 * avg_bpm:
                        print(col.WARNING, bpm, " : ", avg_bpm, col.ENDC)
                    else:
                        print(col.OKGREEN, bpm, " : ", avg_bpm, col.ENDC)
                print("")
if __name__ == '__main__':
    main()
