package body Energy_Algorithm is
   procedure Energy_Algo (New_Value      : in     Energy;
                          Last_Beat      : in out Boolean;
                          Id_Chunk       : in out Integer;  
                          Last_BPMs      : in out BPM_Table;
                          Id_BPM         : in out Integer;
                          Tps, Last_Time : in out Time;    
                          Last_Bad       : in out Boolean;
                          Energy_Total   : in out Energy_Table)
   is    
      Beat                  : Boolean;
      New_Tps, Tmp_Time     : Time;
      Period                : Duration;     
      Avg_BPM, Alt_BPM, BPM : Float     := 0.0;   
      Continue              : Boolean   := False;      
   begin
      Beat := Is_Beat(New_Value, Energy_Total, Id_Chunk);         
      
      Continue := False;
      
      if not Beat then
         if Last_Beat then
            New_Tps := Ada.Real_Time.Clock;
            Period := To_Duration(New_Tps - Tps);
            Tmp_Time := Tps;
            Tps := New_Tps;
            BPM := 1.0 / Float(Period) * 60.0;
            
            Avg_BPM := Compute_Avg_Bpm(Last_BPMs);
            
            if Last_Bad then
               Last_Bad := False;
               Alt_BPM := 1.0 / Float(To_Duration(New_Tps - Last_Time)) * 60.0;
               if abs(Avg_BPM - Alt_BPM) < 0.3 * Avg_BPM then
                  BPM := Alt_BPM;
               else
                  if Alt_BPM > Avg_BPM then
                     Continue := True;
                  end if;
               end if;
            end if;
            
            if not Continue then
               
               if abs(Avg_BPM - BPM) > Float'Max(0.6 * Avg_BPM, 40.0) or else BPM < 40.0 then
                  Last_Bad := False;
                  Last_Time := Tmp_Time;
                  Continue := True;
               end if;
               
               if not Continue then
                  Last_BPMs(Id_BPM) := BPM;
                  Id_BPM := Id_BPM + 1;
                  if Id_BPM >= Inertia_Size then
                     Id_BPM := 0;
                  end if;
                  
                  -- mydraw.Clear;
                  mydraw.Draw_number(Integer'Image(Integer(Avg_BPM)));
                  mydraw.Draw_beat;
                  mydraw.Clear;
                  mydraw.Draw_number(Integer'Image(Integer(Avg_BPM)));
               end if;
            end if;
         end if;
      end if;
      
      Last_Beat := Beat;
      
   end Energy_Algo;

end Energy_Algorithm;
