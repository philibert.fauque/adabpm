with Last_Chance_Handler;  pragma Unreferenced (Last_Chance_Handler);
with STM32.Board;           use STM32.Board;
with HAL.Bitmap;            use HAL.Bitmap;
with STM32.User_Button;     use STM32;
with BMP_Fonts;
with LCD_Std_Out;

package mydraw is

   procedure Draw_number (beat : String);
   procedure Draw_init;
   procedure Clear;
   procedure Draw_beat;
   
end mydraw;
