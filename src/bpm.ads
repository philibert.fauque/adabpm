with Types; use Types;

package Bpm is
   Cst : constant Float := 1.35;

   function Init_Energy_Total return Energy_Table with 
     Post => Init_Energy_Total'Result'Length = Nb_Chunks and then
     (for all E of Init_Energy_Total'Result => E = 0);
   
   function Init_Last_BPMs(Default : Float) return BPM_Table with
     Pre => Default = 100.0,
     Post => Init_Last_BPMs'Result'Length = Inertia_Size and then
     (for all E of Init_Last_BPMs'Result => E = Default);
   
   function Compute_Avg_Bpm(Last_BPMs : BPM_Table) return Float with
     Post => Compute_Avg_Bpm'Result >= 0.0;

   function Is_Beat (Energ        : in     Energy;
                     Energy_Total : in Out Energy_Table;
                     Id_Chunk     : in out Integer)
                     return Boolean;
       
private
   procedure Update_Energy (Energ        : in     Energy;
                            Energy_Total : in Out Energy_Table;
                            Id_Chunk     : in out Integer) with
     Post => (if Id_Chunk < Nb_Chunks - 1 then Id_Chunk = Id_Chunk'Old + 1
              else Id_Chunk = 0);
   
   function Compute_Etot (Energy_Total: in Energy_Table) return Float with 
     Post => Compute_Etot'Result >= 0.0;
     
end Bpm;
