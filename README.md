# AdaBPM

Create in Ada a firmware for STM32 card which will determine the BPM of music.

Based on Nucleo-F429 Discovery.

## Requirements

* Clone the [Ada Drivers Library](https://github.com/AdaCore/Ada_Drivers_Library)
inside this project.
* Download and install this [Toolchain](https://www.adacore.com/download) for ARM.
* Install the `st-link` package
* Install the packages serial and alsaaudio for python
``` bash
$ pip install pyserial pyalsaaudio
```

* Add the user to the uucp group to have access to /dev/ttyUSB0

``` bash
$ sudo usermod -a -G uucp $USER
```

## Usage

You need to connect the board with your computer using UART.
On the Nucleo-F429 Discovery, TX is PM6 and RX is PB7.

To compile and flash the project  on the card, do :

``` bash
make
make flash
```

Then, to start recording the sound and start the communication with the card, do

``` bash
python recorder.py /dev/ttyUSB0
```

The sound is recorded on the computer and sent to the nucleo card.
The BPM calculated will be displayed on the LCD screen.

``` bash
python advanced_recorder.py /dev/ttyUSB0
```

This script works like the last one, but it also displays a comparison
with the python implementation of the algorithm.
