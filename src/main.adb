with uart_nonblocking; use uart_nonblocking;

procedure Main is
begin
   uart_nonblocking.get_uart;
end Main;
