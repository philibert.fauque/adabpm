PREFIX=arm-eabi
OBJCOPY=$(PREFIX)-objcopy

ENTRY_POINT=prj.gpr
OBJDIR=obj
ELF=main
BIN=$(ELF).bin

all:
	gprbuild -P $(ENTRY_POINT)
	$(OBJCOPY) -O binary $(OBJDIR)/$(ELF) $(OBJDIR)/$(BIN)


flash:
	st-flash --reset write $(OBJDIR)/$(BIN) 0x08000000


clean:
	gprclean
	$(RM) -rf $(OBJDIR)
