#!/bin/env python3
import numpy as np
import signal
import sys
import serial
import time
import alsaaudio, time, audioop

from sys import argv

class col:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

stop = False
def signal_handler(sig, frame):
    print(col.WARNING, 'Ctrl+C signal detected', col.ENDC)
    global stop
    stop = True
    sys.exit(130)
signal.signal(signal.SIGINT, signal_handler)

inp = alsaaudio.PCM(alsaaudio.PCM_CAPTURE, alsaaudio.PCM_NORMAL)
inp.setchannels(1)
inp.setrate(20000)
inp.setformat(alsaaudio.PCM_FORMAT_S16_LE)
byte_size = 2

nb_sample = 1024
inp.setperiodsize(nb_sample)

nb_pts = 100
def main():
    if len(argv) < 2:
        print(col.FAIL, 'Missing argument')
        print(col.WARNING, 'Usage: $./recorder.py serial', col.ENDC)
        return
    
    port_serie =  serial.Serial(port=argv[1], baudrate=115200, timeout=1
            , writeTimeout=1)
    if not port_serie.isOpen():
        print(col.FAIL, 'invalid serial port', col.ENDC)
        return

    global stop
    data_str = ''
    print("Start recording.")
    while not(stop):
        # Read data from device
        l, data = inp.read()
        energ = 0
        if l:
            for k in range(nb_sample):
                tmp = data[k:k+2]
                val = abs(audioop.getsample(data, byte_size, k))
                energ += val ** 2

                hex_str = "%0.4X" % val
                data_str += hex_str

            hex_nrg = "%0.16d" % energ
            hex_nrg += '\r\n'
            data_str += '\r\n'
            port_serie.write(hex_nrg.encode())

if __name__ == '__main__':
    main()

