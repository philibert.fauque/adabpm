package body uart_nonblocking is

function atoi (str : String) return Energy is
      energ : Energy    := 0;
      n     : Character := Character'Val (10);
      r     : Character := Character'Val (13);

   begin
      for I in str'Range loop
         if str(I) /= n and then str(I) /= r then
            energ := energ * 10;
            energ := energ + Energy(Integer'Value((1 => str(I))));
         end if;
      end loop;
      return energ;
   end;


 procedure get_uart is
      Incoming : aliased Message (Physical_Size => 1024);  -- arbitrary size

   procedure Send (This : String);

   procedure Send (This : String) is
         Outgoing : aliased Message (Physical_Size => 1024);  -- arbitrary size
      begin
         Set (Outgoing, To => This);
         Put (COM, Outgoing'Unchecked_Access);
         Await_Transmission_Complete (Outgoing);
         --  We must await xmit completion because Put does not wait
      end Send;

      New_Value      : Energy;
      Id_Chunk       : Integer := 0;

      Last_BPMs      : BPM_Table;
      Id_BPM         : Integer := 0;

      Tps, Last_Time : Time;

      Last_Beat      : Boolean := False;
      Last_Bad       : Boolean := False;
      Energy_Total   : Energy_Table;

   begin
      Energy_Total := Init_Energy_Total;
      Last_BPMs := Init_Last_BPMs(100.0);
      Tps := Ada.Real_Time.Clock;
      mydraw.Draw_init;
      Initialize (COM);

      Configure (COM, Baud_Rate => 115_200);

      Set_Terminator (Incoming, To => ASCII.CR);
      loop

         Get (COM, Incoming'Unchecked_Access);
         Await_Reception_Complete (Incoming);
         New_Value := atoi(Content(Incoming));
         Energy_Algorithm.Energy_Algo(New_Value, Last_Beat, Id_Chunk, Last_BPMs,
                                      Id_BPM, Tps, Last_Time, Last_Bad, Energy_Total);

      end loop;
   end get_uart;
end uart_nonblocking;
