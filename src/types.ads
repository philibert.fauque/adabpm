package Types is
   Nb_Samples   : constant Integer := 1024;
   Nb_Chunks    : constant Integer := 44;
   Inertia_Size : constant Integer := 15;
   
   type Sound is new Integer;
   type Sound_Samples is array(0 .. Nb_Samples)   of Sound;
   subtype Energy is Long_Long_Integer
     with Static_Predicate => Energy >= 0 and Energy < 2 ** 40; -- 10^16
   type Energy_Table  is array(0 .. Nb_Chunks)    of Energy;
   type BPM_Table     is array(0 .. Inertia_Size) of Float;
end Types;
